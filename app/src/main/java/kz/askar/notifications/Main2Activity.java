package kz.askar.notifications;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        String result = getIntent().getStringExtra("msg");
        TextView msgTV = (TextView) findViewById(R.id.msg);
        msgTV.setText(result);
    }
}
