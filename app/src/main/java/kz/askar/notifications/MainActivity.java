package kz.askar.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(this, Main2Activity.class);
        intent.setAction("Cancel");
        intent.putExtra("msg", "Cancel");
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intent2 = new Intent(this, Main2Activity.class);
        intent2.setAction("OK");
        intent2.putExtra("msg", "We want to press OK");
        PendingIntent pIntent2 = PendingIntent.getActivity(this, 0, intent2,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(android.R.drawable.stat_notify_chat)
                .setContentTitle("Tiltle")
                .setContentText("Text")
                .addAction(android.R.drawable.ic_menu_search, "OK", pIntent2)
                .addAction(android.R.drawable.ic_menu_search, "Cancel", pIntent)
                .setVisibility(Notification.VISIBILITY_PRIVATE);

        NotificationCompat.InboxStyle inbox = new NotificationCompat.InboxStyle();
        inbox.setBigContentTitle("Big big big content content");
        inbox.addLine("Line 1 Line 1 Line 1 Line 1 Line 1 Line 1");
        inbox.addLine("Line 1 Line 1 Line 1 Line 1 Line 1 Line 1");
        inbox.addLine("Line 1 Line 1 Line 1 Line 1 Line 1 Line 1");
        inbox.addLine("Line 1 Line 1 Line 1 Line 1 Line 1 Line 1");
        builder.setStyle(inbox);


        NotificationManager nm = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        nm.notify(0, builder.build());
    }
}
