package kz.askar.notif_group2;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(this, Activity2.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(Activity2.class);
        stackBuilder.addNextIntent(intent);

        PendingIntent pIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);


        Intent intent2 = new Intent(this, Activity2.class);
        intent2.setAction("OK");
        intent2.putExtra("msg", "OK was pressed");
        TaskStackBuilder stackBuilder2 = TaskStackBuilder.create(this);
        stackBuilder2.addParentStack(Activity2.class);
        stackBuilder2.addNextIntent(intent2);

        PendingIntent pIntent2 = stackBuilder2.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);


        Intent intent3 = new Intent(this, Activity2.class);
        intent3.setAction("Cancel");
        intent3.putExtra("msg", "Cacelled");
        TaskStackBuilder stackBuilder3 = TaskStackBuilder.create(this);
        stackBuilder3.addParentStack(Activity2.class);
        stackBuilder3.addNextIntent(intent3);

        PendingIntent pIntent3 = stackBuilder3.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);

//        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
//                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(android.R.drawable.ic_menu_search)
                .setContentTitle("Title")
                .setContentText("Text")
                .setContentIntent(pIntent)
                .addAction(android.R.drawable.ic_menu_search,"OK", pIntent2)
                .addAction(android.R.drawable.ic_menu_search,"Cancel", pIntent3)
                .setAutoCancel(true)
                .setVisibility(Notification.VISIBILITY_PRIVATE);

        NotificationCompat.InboxStyle inbox = new NotificationCompat.InboxStyle();
        inbox.setBigContentTitle("Super big content");
        inbox.addLine("Text text textText text textText text textText text textText text text");
        inbox.addLine("Text text textText text textText text textText text textText text text");
        inbox.addLine("Text text textText text textText text textText text textText text text");
        inbox.addLine("Text text textText text textText text textText text textText text text");
        inbox.addLine("Text text textText text textText text textText text textText text text");
        builder.setStyle(inbox);

        NotificationManager nm = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);


        nm.notify(0, builder.build());
    }
}
